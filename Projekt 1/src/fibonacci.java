/**
 * Created by Krzysztof on 2017-03-05.
 */
package zad_1;
public class fibonacci {
    // recursive declaration of method fibonacci
    public static long fib(long number) {
        if ((number == 0) || (number == 1)) // base cases
            return number;
        else
            // recursion step
            return fib(number - 1) + fib(number - 2);
    }

    public static void main(String[] args) {
        long n=0;
        int suma=0;
        while(fib(n)<4000000) {
            if (fib(n)%2==0)
            {
                suma+=fib(n);
            }
            n++;
        }
        System.out.println(suma);
        }
}